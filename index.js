/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
const parser = require('./parser.js');

exports.server = async(req, res) => {
	try{
		const url = req.query.url;

		console.log('url', url);

		let result = await parser.parseWithPuppeteer(url);

		if (result.title) {
			res.status(200).send(result);
		}
		else {
			res.status(500).send(result);
		}
	}
	catch(err) {
		res.status(500).send({"success": false, "error": "Something bad happened when reading this page"});
	}
	
};
