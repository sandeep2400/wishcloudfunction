'use strict';

const puppeteer = require('puppeteer');
const UserAgent = require('user-agents');

var parser = {
	getTitle: async function(page) {
		let names = [];
		let title = await page.title();

		if (title !== null) {
			names.push(title);
		}

		if (await page.$('head > meta[property="og:title"]') !== null) {
			const text = await page.$eval('head > meta[property="og:title"]', element => {
				if ((element) && typeof element != 'undefined') {
					return element.content;
				}
			});		
			names.push(text);
		}
		return names;
	},

	companyDomainLookup: function() {
		var array = [
			{ name: "Amazon.com", domain: "www.amazon.com" },
			{ name: "Best Buy", domain: "www.bestbuy.com" },
			{ name: "Apple", domain: "www.apple.com" },
			{ name: "GameStop", domain: "www.gamestop.com" },
			{ name: "Barnes And Noble", domain: "www.barnesandnoble.com" },
			{ name: "QVC", domain: "www.qvc.com" },
			{ name: "Bath And Body Works", domain: "www.bathandbodyWorks.com" },
			{ name: "Sally Beauty", domain: "www.sallybeauty.com" },
			{ name: "Sephora", domain: "www.Sephora.com" },
			{ name: "Michaels", domain: "www.michaels.com" },
			{ name: "Hobby Lobby", domain: "www.hobbylobby.com" },
			{ name: "Jo-Ann Fabric and Craft Stores", domain: "www.joann.com" },
			{ name: "A.C. Moore Arts & Crafts", domain: "www.acmoore.com" },
			{ name: "Ikea", domain: "www.ikea.com" },
			{ name: "Home Depot", domain: "www.homedepot.com" },
			{ name: "Lowe's", domain: "www.lowes.com" },
			{ name: "Ace Hardware", domain: "www.acehardware.com" },
			{ name: "Menards", domain: "www.menards.com" },
			{ name: "Staples", domain: "www.staples.com" },
			{ name: "Walmart", domain: "www.walmart.com" },
			{ name: "Target", domain: "www.target.com" },
			{ name: "Costco", domain: "www.costco.com" },
			{ name: "Macy's", domain: "www.macys.com" },
			{ name: "Newegg", domain: "www.newegg.com" },
			{ name: "eBay", domain: "www.ebay.com" },
			{ name: "Overstock", domain: "www.overstock.com" },
			{ name: "Kohls", domain: "www.kohls.com" },
			{ name: "Nike", domain: "www.nike.com" },
			{ name: "Nike", domain: "www.nike.com" }		
		];

		return array;
	},

	getCompany: async function(page) {
		let company = [];

		if (await page.$('head > meta[property="og:site_name"]') !== null) {
			const text = await page.$eval('head > meta[property="og:site_name"]', element => element.content );		
			company.push(text);
		}

		if (await page.$('head > meta[name="application-name"]') !== null) {
			const text = await page.$eval('head > meta[name="application-name"]', element => element.content);
			if (text !== null) {
				company.push(text);
			}
		}		

		if (await page.$(".logo") !== null) {
			const text = await page.$eval(".logo", element => element.getAttribute('title'));
			if (text !== null) {
				company.push(text);
			}
		}

		if (await page.$("#logo") !== null) {
			const text = await page.$eval("#logo", element => element.getAttribute('title'));
			if (text !== null) {
				company.push(text);
			}
		}

		if (await page.$("a[href='/']") !== null) {
			const text = await page.$eval("a[href='/']", element => element.getAttribute('title'));
			if (text !== null) {
				company.push(text);
			}
		}

		var hostname = await page.evaluate(() => { return window.location.hostname;});
		var lookup = parser.companyDomainLookup();
		let result = lookup.filter(business => {
			return (business.domain == hostname);
		});


		if (result && result.length) {
			company.push(result[0].name);
		}

		if (hostname.split(".")[0] == "www") {
			company.push(hostname.split(".")[1]);
		}
		else {
			company.push(hostname.split(".")[0]);
		}
		company.push(hostname);

		return company;
	},

	proximatePricesAreTheSame: function(curr, next) {
		return (curr['text'] === next['text']);
	},

	proximatePricesAreVeryClose: function(curr, next) {
		return (
				( Math.abs(curr['y']) - Math.abs(next['y']) < 50) ||
				( Math.abs(curr['x']) - Math.abs(next['x']) < 50)		
			)
	},

	proximatePriceFontsAreDiff: function(curr, next) {
		return (curr['fontSize'] !== next['fontSize']);
	},

	priceMissingDecimal: function(price) {
		let reDot = /[.]/g;
		return (price.search(reDot) == -1);
	},

	fixDecimalFormatting: async function(priceRecs) {
		for (var i = priceRecs.length - 1; i >= 1;  i--) {
			let curr = priceRecs[i];
			let next = priceRecs[i-1];

			if ( parser.priceMissingDecimal(curr['text']) &&
				 parser.priceMissingDecimal(next['text']) && 
				 parser.proximatePricesAreTheSame(curr, next) && 
				 parser.proximatePricesAreVeryClose(curr, next) &&
				 parser.proximatePriceFontsAreDiff(curr, next) && 
				 (curr['text'].length > 2)
			   ) {
			   	let priceToArr = curr['text'].split('');
			   	priceToArr.splice(priceToArr.length -2 , 0, '.');
			   	priceRecs[i-1]['text'] = priceToArr.join('');
			   	priceRecs[i]['text'] = priceToArr.join('');
			}
		}
		return priceRecs;
	},

	canBePrice: function(record) {
		if ( record['y'] > 600 ||
	         record['fontSize'] == undefined ||
	         !record['text'].match(/(^(rs\.|Rs\.|RS\.|\$|₹|INR|INR ₹|USD|US |US \$|CAD|C\$){1}(\s){0,1}[\d,]+(\.\d+){0,1}(\s){0,1}$)/)
	         ){
			return false
		}
		else {
			return true;
		}
	},

	cleanNumberValues: function (price) {
		if ((parseFloat(price['text'].replace(/(rs\.|Rs\.|RS\.|\$|₹|INR|INR ₹|USD|US \$|CAD|C\$)/ig,"")) > 1.00) && (price['text'].length <= 15)) {
			return true;
		}
		else {
			return false;
		}		
	},

	removeBlanks: function(record) {
		return (record['text'].length > 0 )
	},

	getPrice: async function(page) {
		try {
			let records = await page.$$eval('body *', elements => {
				return elements.map(element => {
					var block = {};
					let bBox = element.getBoundingClientRect();
					let boxText = element.textContent.trim();
					let priceRegex = /((rs\.|Rs\.|RS\.|\$|₹|INR|INR ₹|USD|US |US \$|CAD|C\$){1}(\s){0,1}[\d,]+(\.\d+){0,1}(\s){0,1}$)/;

					let text = '';

					// let matchArray = priceRegex.exec(boxText);
					
					// if (matchArray && matchArray.length) {
					// 	text = matchArray[0];
					// }

					text = boxText;

					if ((text.length > 2) && (text.length <= 30) && !(bBox.x == 0 && bBox.y == 0)) {
						block['fontSize'] = parseInt(getComputedStyle(element)['fontSize']);
					}

					block['y'] = bBox.y;
					block['x'] = bBox.x; 
					block['text'] = text;					
					return block;
				})
			});

			let nonBlankRecords = records.filter(parser.removeBlanks);

			let possiblePriceRecords = records.filter(parser.canBePrice);

			let decimalFixedPrices = await parser.fixDecimalFormatting(possiblePriceRecords);
			
			let cleanPrices = await decimalFixedPrices.filter(parser.cleanNumberValues);

			let priceRecsSortedByFontSize = cleanPrices.sort(function(a, b) {
				if (a['fontSize'] == b['fontSize']) return a['y'] < b['y']
					return (b['fontSize'] - a['fontSize']);
			});
			let sortedprices = [];
			priceRecsSortedByFontSize.forEach(function(rec) {
				sortedprices.push(rec['text'].replace(/(rs\.|Rs\.|RS\.|\$|₹|INR|INR ₹|USD|US \$|USD|CAD|C\$)/ig,""));
			});			
			let uniquePrices = [];
			uniquePrices = [...new Set(sortedprices)];	

			return uniquePrices.slice(0, 4);
		}
		catch(error) {
			throw error;
		}
	},

	extractImages: async function(page) {
		try {
			let images = await page.$$eval('img', elements => {
				
				return elements.map(element => {
					var record = {};
					let bBox = element.getBoundingClientRect();
					if (!(bBox.x <= 0 && bBox.y <= 0) && (bBox.width >=250 || bBox.height >=250) && ((bBox.y + bBox.height) > 160)) {
						record['x'] = bBox.x;
						record['y'] = bBox.y;
						record['width'] = bBox.width;
						record['height'] = bBox.height;
						record['src'] = element.currentSrc;
					}
					return record;
				})
			});
			let possibleHeroImages = images.filter(parser.canBeHeroes);
			let imagesSortedByHeight = possibleHeroImages.sort((a, b) => (a['height'] < b['height']) ? 1 : -1);
			let sortedImages = [];
			imagesSortedByHeight.forEach(function(img) {
				sortedImages.push(img['src']);
			})
			return sortedImages.slice(0,3);
		}
		catch(error) {
			throw error;
		}
		
	},

	isValidHttpUrl: function(string) {
		let url = string;
		try {
			url = new URL(string);
		}
		catch(_) {
			return false;
		}

		return url.protocol === "http:" || url.protocol === "https:";
	},

	canBeHeroes: function(record) {
		if ((!record) || (!record.src) || ((record['y'] + record['height']) <= 160) || (record.height <= 50) || (!parser.isValidHttpUrl(record.src))) {
			return false;
		}
		else {
			return true;
		}
	},

	scrapeAmazonPages: async function (url) {
		let scraperapiClient = require('scraperapi-sdk')('79544a7f6ffad09a5c68c687d4c83f4a')
		let response = await scraperapiClient.get(url, {render: true});
		return response;
	},	

	isRequestedUrlAmazon: function(url) {
		return url.toLowerCase().includes("amazon");
	}, 

	getAmazonCompany: function() {
		let company = [];
		company.push("Amazon.com");
		return company;
	},

	parseWithPuppeteer: async function(url) {
		try {
			//open the headless browser
			let details = {};
			const browser = await puppeteer.launch({
				headless: true,
				timeout: 9000,
				args: ['--no-sandbox']
			});
			const page = await browser.newPage();

			if (parser.isRequestedUrlAmazon(url)) {
				let htmlSource  = await parser.scrapeAmazonPages(url);	

				await page.setContent(htmlSource);
			}
			else {
				const userAgent = new UserAgent();
				// set user agent (override the default headless User Agent)
	        	await page.setUserAgent(userAgent.toString());
				await page.goto(url, { waitUntil: ["networkidle2"] });
				await page.setViewport({width: 1920, height: 1080});
			}


			details.title = await parser.getTitle(page);
			details.price = await parser.getPrice(page);
			details.pics  = await parser.extractImages(page);

			if (parser.isRequestedUrlAmazon(url)) {
				details.company = parser.getAmazonCompany();
			}
			else {
				details.company =  await parser.getCompany(page);
			}
			details.success = true;
			console.log('details', details);

			await browser.close();	
			return details;
		}
		catch(err) {
			console.log('error: ', err.message);
			return {"success": false, "error": err.message};
		};
	}
};

module.exports = parser;