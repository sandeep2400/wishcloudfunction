#! /bin/sh
# Command to deploy the WishParseFunction
# 
gcloud functions deploy WishParseFunction --runtime=nodejs12 --allow-unauthenticated --entry-point=server --memory=1024MB --timeout=60s --trigger-http